//
//  FriendRequestCell.swift
//  AIScribe
//
//  Created by Randall Ridley on 8/23/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class FriendRequestCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var friendIV: UIImageView!
    @IBOutlet weak var requestIV: UIImageView!
}
