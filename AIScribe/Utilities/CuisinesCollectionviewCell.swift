//
//  CuisinesCollectionviewCell.swift
//  AIScribe
//
//  Created by Randall Ridley on 5/19/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class CuisinesCollectionviewCell: UICollectionViewCell {
    
    //@IBOutlet weak var cuisineBtn: UIButton!
    @IBOutlet weak var cuisineLbl: UILabel!
    @IBOutlet weak var iconIV: UIImageView!
    
}
